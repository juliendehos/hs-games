{-# Language FunctionalDependencies #-}

module Cli where
 
import Control.Monad (forM_)
import Data.List (intercalate)
import Text.Read (readMaybe)

import qualified Data.Vector as V

import qualified Game.Find5 as F
import qualified Game.Tictactoe as T

class Cli g a s | g -> a s where
    printGame :: g a s -> IO ()
    readAction :: g a s -> IO (Maybe a)

instance Cli F.Find5 F.Action F.Status where

    printGame g = do
        print (F.unAction <$> F._actions g) 
        print (F._status g)

    readAction _ = do
        mi <- readMaybe <$> getLine
        return $ F.Action <$> mi

instance Cli T.Tictactoe T.Action T.Status where

    printGame g = do
        let fmtCell T.Cell0 = "."
            fmtCell T.Cell1 = "1"
            fmtCell T.Cell2 = "2"
            actions = V.toList $ T._actions g
        putStrLn ""
        forM_ [0..2] $ \i -> do
            forM_ [0..2] $ \j -> do
                putStr $ fmtCell $ T._board g V.! T.ijToK i j
            putStrLn ""
        print (T._status g)
        putStrLn $ intercalate ", " [show i ++ " " ++ show j | (T.Action i j) <- actions]

    readAction _ = do
        ns <- map readMaybe . words <$> getLine
        return $ case ns of
            [Just i, Just j] -> Just $ T.Action i j
            _ -> Nothing


