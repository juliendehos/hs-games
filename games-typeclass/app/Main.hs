import Control.Monad ( when)
import System.Environment (getArgs)
import System.IO (hFlush, stdout)

import Cli
import qualified Game as G
import qualified Game.Find5 as F
import qualified Game.Tictactoe as T

usage :: IO ()
usage = putStrLn 
    "usage: <game>\n\n\
    \game: find5 tictactoe "

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["find5"] -> run F.mkFind5
        ["tictactoe"] -> run T.mkTictactoe
        _ -> usage

run :: (Eq a, Cli g a s, G.Game g a s) => g a s -> IO ()
run g = do
    printGame g
    when (G.isRunning g) $ do
        putStr "n? "
        hFlush stdout
        ma <- readAction g
        run (maybe g (`G.playAction` g) ma)

