{-# Language FunctionalDependencies #-}

module Game where

import qualified Data.Vector as V

class Game g a s | g -> a s where
    playAction :: Eq a => a -> g a s -> g a s
    playActionN :: Int -> g a s -> g a s
    isRunning :: g a s -> Bool
    isTerminated :: g a s -> Bool
    getStatus :: g a s -> s
    getActions :: g a s -> V.Vector a

    isTerminated = not . isRunning

    playAction a g =
        case V.findIndex (==a) (getActions g) of
            Nothing -> g
            Just n -> playActionN n g

