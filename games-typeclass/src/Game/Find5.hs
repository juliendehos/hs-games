{-# Language MultiParamTypeClasses #-}

module Game.Find5 where

import qualified Data.Vector as V

import Game

data Status = Play | Win | Lose
    deriving (Eq, Show)

newtype Action = Action { unAction :: Int }
    deriving (Eq, Show)

data Find5 a s = Find5
    { _actions :: V.Vector a
    , _status :: Status
    }

mkFind5 :: Find5 Action Status
mkFind5 = Find5 (Action <$> V.fromList [1..10]) Play

instance Game Find5 Action Status where

    isRunning g = _status g == Play

    getStatus = _status

    getActions = _actions 

    playActionN n g@(Find5 actions _)
        | n < 0 || n >= V.length actions || isTerminated g = g
        | otherwise = 
            let actions' = V.ifilter (\ n' _ -> n' /= n) actions
                status' | Action 5 `V.notElem` actions' = Win
                        | V.length actions' <= 5 = Lose
                        | otherwise = Play
            in Find5 actions' status'

