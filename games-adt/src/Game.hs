module Game where

import qualified Game.Find5 as F
import qualified Game.Tictactoe as T

data Game 
    = GameFind5 F.Find5
    | GameTictactoe T.Tictactoe

data Action
    = ActionFind5 F.Action
    | ActionTictactoe T.Action

isRunning :: Game -> Bool
isRunning (GameFind5 g) = F.getStatus g == F.Play
isRunning (GameTictactoe g) = T.isRunning g

playAction :: Action -> Game -> Game
playAction (ActionFind5 a) (GameFind5 g) = GameFind5 $ F.playAction a g
playAction (ActionTictactoe a) (GameTictactoe g) = GameTictactoe $ T.playAction a g
playAction _ _ = error "incompatible game-action"

