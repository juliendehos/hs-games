module Game.Tictactoe where

import qualified Data.Vector as V

data Player = Player1 | Player2
    deriving (Eq)

data Status = Play1 | Play2 | Win1 | Win2 | Tie 
    deriving (Eq, Show)

data Action = Action Int Int 
    deriving (Eq)

data Cell = Cell0 | Cell1 | Cell2 
    deriving (Eq)

data Tictactoe = Tictactoe 
    { _player :: Player
    , _status :: Status
    , _actions :: V.Vector Action
    , _board :: V.Vector Cell
    }

mkTictactoe :: Tictactoe
mkTictactoe = 
    let board = V.replicate 9 Cell0
        actions = V.fromList [Action i j | i<-[0..2], j<-[0..2]]
    in Tictactoe Player1 Play1 actions board

isRunning :: Tictactoe -> Bool
isRunning (Tictactoe _ status _ _) = status == Play1 || status == Play2

nextPlayer :: Player -> Player
nextPlayer Player1 = Player2
nextPlayer Player2 = Player1

playerToCell :: Player -> Cell
playerToCell Player1 = Cell1
playerToCell Player2 = Cell2

ijToK :: Int -> Int -> Int
ijToK i j = i*3 + j

checkBoard :: V.Vector Cell -> Int -> Int -> Bool
checkBoard b i j =
    let k = ijToK i j
        c = b V.! k
        check = all (\(ii,jj) -> b V.! ijToK ii jj == c)
    in check [(i,0), (i,1), (i,2)] ||
       check [(0,j), (1,j), (2,j)] ||
       check [(0,0), (1,1), (2,2)] || 
       check [(0,2), (1,1), (2,0)]

playAction :: Action -> Tictactoe -> Tictactoe
playAction a g = 
    case V.findIndex (==a) (_actions g) of
        Nothing -> g
        Just n -> playActionN n g

playActionN :: Int -> Tictactoe -> Tictactoe
playActionN n g@(Tictactoe player _ actions board)
    | n < 0 || n >= V.length actions || not (isRunning g) || cell /= Cell0 = g
    | otherwise = Tictactoe player' status' actions' board'
    where 
        a@(Action i j) = actions V.! n
        k = ijToK i j
        cell = board V.! k
        board' = board V.// [(k, playerToCell player)]
        actions' = V.filter (/=a) actions
        player' = nextPlayer player
        status'
            | checkBoard board' i j = if player==Player1 then Win1 else Win2
            | V.null actions' = Tie
            | otherwise = if player==Player1 then Play2 else Play1

