module Game.Find5 where

import qualified Data.Vector as V

data Status = Play | Win | Lose
    deriving (Eq, Show)

newtype Action = Action { unAction :: Int }
    deriving (Eq, Show)

newtype Find5 = Find5
    { _actions :: V.Vector Action
    }

mkFind5 :: Find5
mkFind5 = Find5 (Action <$> V.fromList [1..10])

getStatus :: Find5 -> Status
getStatus (Find5 actions) 
    | Action 5 `V.notElem` actions = Win
    | V.length actions <= 5 = Lose
    | otherwise = Play

playAction :: Action -> Find5 -> Find5
playAction a g@(Find5 actions)
    | getStatus g /= Play = g
    | otherwise = Find5 (V.filter (/=a) actions)

playActionN :: Int -> Find5 -> Find5
playActionN n g@(Find5 actions)
    | n < 0 || n >= V.length actions || getStatus g /= Play = g
    | otherwise = Find5 (V.ifilter (\ n' _ -> n' /= n) actions)

