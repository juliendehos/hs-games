module Cli where

import Control.Monad (forM_)
import Data.List (intercalate)
import Text.Read (readMaybe)

import qualified Data.Vector as V

import qualified Game as G
import qualified Game.Find5 as F
import qualified Game.Tictactoe as T

printGame :: G.Game -> IO ()
printGame (G.GameFind5 g) = do
    print (F.unAction <$> F._actions g) 
    print (F.getStatus g)
printGame (G.GameTictactoe g) = do
    let fmtCell T.Cell0 = "."
        fmtCell T.Cell1 = "1"
        fmtCell T.Cell2 = "2"
        actions = V.toList $ T._actions g
    putStrLn ""
    forM_ [0..2] $ \i -> do
        forM_ [0..2] $ \j -> do
            putStr $ fmtCell $ T._board g V.! T.ijToK i j
        putStrLn ""
    print (T._status g)
    putStrLn $ intercalate ", " [show i ++ " " ++ show j | (T.Action i j) <- actions]

readAction :: G.Game -> IO (Maybe G.Action)
readAction (G.GameFind5 _) = do
    mi <- readMaybe <$> getLine
    return $ G.ActionFind5 . F.Action <$> mi
readAction (G.GameTictactoe _) = do
    ns <- map readMaybe . words <$> getLine
    return $ case ns of
        [Just i, Just j] -> Just $ G.ActionTictactoe $ T.Action i j
        _ -> Nothing

