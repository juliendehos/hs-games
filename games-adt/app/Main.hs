
import Control.Monad (when)
import System.Environment (getArgs)
import System.IO (hFlush, stdout)

import Cli

import qualified Game as G
import qualified Game.Find5 as F
import qualified Game.Tictactoe as T

usage :: IO ()
usage = putStrLn 
    "usage: <game>\n\n\
    \game: find5 tictactoe "

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["find5"] -> run (G.GameFind5 F.mkFind5)
        ["tictactoe"] -> run (G.GameTictactoe T.mkTictactoe)
        _ -> usage

run :: G.Game -> IO ()
run g = do
    printGame g
    when (G.isRunning g) $ do
        putStr "n? "
        hFlush stdout
        ma <- readAction g
        run (maybe g (`G.playAction` g) ma)

